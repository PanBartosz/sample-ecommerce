package com.ruszniak.sggwshopexample.core.exception;

import lombok.*;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
class ApiError {
    private ErrorCode errorCode;
    private String errorMessage;
}
