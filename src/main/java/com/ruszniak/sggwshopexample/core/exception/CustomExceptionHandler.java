package com.ruszniak.sggwshopexample.core.exception;

import com.ruszniak.sggwshopexample.core.validation.FailedValidationExaction;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(FailedValidationExaction.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<ApiError> validationErrors(FailedValidationExaction ex) {
        return ex.getViolations()
                .stream()
                .map(violation ->
                        ApiError
                                .builder()
                                .errorCode(ErrorCode.ValidationError)
                                .errorMessage(violation.getMessage())
                                .build())
                .collect(Collectors.toList());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError validationError(RuntimeException ex) {
        return ApiError
                .builder()
                .errorCode(ErrorCode.InternalServerError)
                .errorMessage(ex.getMessage())
                .build();
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError validationError(ObjectNotFoundException ex) {
        return ApiError
                .builder()
                .errorCode(ErrorCode.NotFound)
                .build();
    }
}
