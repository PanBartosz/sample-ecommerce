package com.ruszniak.sggwshopexample.core.exception;

enum ErrorCode {
    ValidationError,
    InternalServerError,
    NotFound
}
