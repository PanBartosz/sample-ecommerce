package com.ruszniak.sggwshopexample.core.validation;

import com.ruszniak.sggwshopexample.core.dto.Request;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.ConstraintViolation;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
public class FailedValidationExaction extends RuntimeException {
    private Set<ConstraintViolation<Request>> violations;
}
