package com.ruszniak.sggwshopexample.core.validation;

import com.ruszniak.sggwshopexample.core.dto.Request;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ValidationProcessor {

    private final Validator validator;

    public void processRequest(Request request) {
        validate(request);
        validate(request, ValidationGroups.PreProcess.class);
        validate(request, ValidationGroups.Business.class);
    }

    private void validate(Request request, Class... groups) {
        Set<ConstraintViolation<Request>> validate = validator.validate(request, groups);
        if (!validate.isEmpty()) {
            throw new FailedValidationExaction(validate);
        }
    }
}
