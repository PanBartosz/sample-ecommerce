package com.ruszniak.sggwshopexample.orders.web.rest;

import com.ruszniak.sggwshopexample.orders.domain.OrderFacade;
import com.ruszniak.sggwshopexample.orders.domain.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
class OrderResource {

    private final OrderFacade orderFacade;

    @GetMapping("orders")
    List<GetOrderResponse> getAllOrders() {
        return orderFacade.getAll();
    }

    @GetMapping("orders/{id}")
    ResponseEntity<GetOrderResponse> getOrder(@PathVariable Long id) {
        return ResponseEntity.ok(orderFacade.getById(id));
    }

    @PostMapping("orders")
    ResponseEntity<CreateOrderResponse> createOrder(@RequestBody CreateOrderRequest request) {
        return ResponseEntity.ok(orderFacade.placeAnOrder(request));
    }

    @PutMapping("orders/invoice")
    ResponseEntity<Invoice> invoiceOrder(InvoiceOrderRequest request) {
        return ResponseEntity.ok(orderFacade.invoiceOrder(request));
    }

    @PutMapping("orders/close")
    ResponseEntity closeOrder(CloseOrderRequest request) {
        orderFacade.closeOrder(request);
        return ResponseEntity.ok().build();
    }
}
