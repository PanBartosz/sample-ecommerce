package com.ruszniak.sggwshopexample.orders.domain.dto;

import com.ruszniak.sggwshopexample.core.dto.Request;
import com.ruszniak.sggwshopexample.core.validation.ValidationGroups;
import com.ruszniak.sggwshopexample.orders.domain.validators.EnoughItemsInStock;
import com.ruszniak.sggwshopexample.orders.domain.validators.ItemExists;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@EnoughItemsInStock(groups = ValidationGroups.Business.class)
@ItemExists(groups = ValidationGroups.PreProcess.class)
public class OrderedItemRequest implements Request {

    private long id;

    @Min(0)
    @NotNull
    private long quantity;
}
