package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.orders.domain.dto.*;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class OrderFacade {

    private final OrderService orderService;

    public List<GetOrderResponse> getAll() {
        return orderService.getAll();
    }

    public GetOrderResponse getById(Long id) {
        return orderService.getById(id);
    }

    public CreateOrderResponse placeAnOrder(CreateOrderRequest request) {
        return orderService.placeAnOrder(request);
    }

    public Invoice invoiceOrder(InvoiceOrderRequest request) {
        return orderService.invoiceOrder(request);
    }

    public void closeOrder(CloseOrderRequest request) {
        orderService.closeOrder(request);
    }
}
