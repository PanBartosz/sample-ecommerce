package com.ruszniak.sggwshopexample.orders.domain.dto;

import com.ruszniak.sggwshopexample.orders.domain.entity.OrderStatus;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GetOrderResponse {

    private String address;

    private BigDecimal price;

    private OrderStatus orderStatus;

    @Builder.Default
    private List<OrderedItemResponse> orderedItemResponse = new ArrayList<>();
}
