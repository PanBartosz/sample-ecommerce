package com.ruszniak.sggwshopexample.orders.domain.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    private String address;

    private BigDecimal price;
}
