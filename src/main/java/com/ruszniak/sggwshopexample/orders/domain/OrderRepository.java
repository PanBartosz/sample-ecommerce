package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.orders.domain.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
