package com.ruszniak.sggwshopexample.orders.domain.entity;

public enum OrderStatus {
    CREATED,
    INVOICED,
    CLOSED
}
