package com.ruszniak.sggwshopexample.orders.domain.validators;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemRequest;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class EnoughItemsInStockValidator implements ConstraintValidator<EnoughItemsInStock, OrderedItemRequest> {

    private final CatalogFacade catalogFacade;

    public boolean isValid(OrderedItemRequest request, ConstraintValidatorContext ctx) {
        Long quantityInStore = catalogFacade.getItemById(request.getId()).getQuantity();
        Long requestQuantity = request.getQuantity();
        return quantityInStore >= requestQuantity;
    }
}
