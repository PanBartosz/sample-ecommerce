package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderEntity;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderedItemEntity;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

class DiscountCalculationService { // todo rethink this

    BigDecimal calculateDiscountedPrice(OrderEntity orderEntity) {
        Set<ItemEntity> items = orderEntity.getOrderedItemEntities()
                .stream()
                .map(OrderedItemEntity::getItemEntity)
                .collect(Collectors.toSet());

        BigDecimal totalPrice = items.stream()
                .map(ItemEntity::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        for (ItemEntity itemEntity : items) {
            for (DiscountEntity discountEntity : itemEntity.getDiscountEntities()) {
                totalPrice = discountEntity.getDiscountStrategy().apply(itemEntity.getPrice(), discountEntity.getAmount(), totalPrice);
            }
        }

        return totalPrice;
    }
}
