package com.ruszniak.sggwshopexample.orders.domain.entity;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ordered_items")
public class OrderedItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private BigDecimal price;

    private long quantity;

    @ManyToOne
    @JoinColumn(name = "orders_id")
    private OrderEntity orderEntity;

    @ManyToOne
    @JoinColumn(name = "items_id", nullable = false)
    private ItemEntity itemEntity;

    @ManyToMany
    @JoinTable(
            name = "ordered_items_discounts",
            joinColumns = @JoinColumn(name = "ordered_items_id"),
            inverseJoinColumns = @JoinColumn(name = "discounts_id")
    )
    private Set<DiscountEntity> discountEntities = new HashSet<>();
}
