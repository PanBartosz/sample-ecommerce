package com.ruszniak.sggwshopexample.orders.domain.dto;

import com.ruszniak.sggwshopexample.core.dto.Request;
import lombok.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@Valid
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderRequest implements Request {

    private String address;

    @Valid
    private Set<OrderedItemRequest> orderedItems = new HashSet<>();
}
