package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.core.validation.ValidationProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class OrderConfig {

    @Bean
    OrderFacade getOrderFacade(
            OrderRepository orderRepository,
            ValidationProcessor validationProcessor,
            CatalogFacade catalogFacade) {

        return new OrderFacade(
                new OrderService(
                        orderRepository,
                        new OrderResponseMapper(),
                        new DiscountCalculationService(),
                        new OrderedItemsService(
                                catalogFacade,
                                new ItemsProvider(
                                        catalogFacade)),
                        validationProcessor));
    }
}
