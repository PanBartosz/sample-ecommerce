package com.ruszniak.sggwshopexample.orders.domain.validators;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemRequest;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class ItemExistsValidator implements ConstraintValidator<ItemExists, OrderedItemRequest> {

    private final CatalogFacade catalogFacade;

    @Override
    public boolean isValid(OrderedItemRequest request, ConstraintValidatorContext constraintValidatorContext) {
        return catalogFacade.itemExistsById(request.getId());
    }
}
