package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemRequest;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderEntity;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderedItemEntity;
import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
class OrderedItemsService {

    private final CatalogFacade catalogFacade;
    private final ItemsProvider itemsProvider;

    Set<OrderedItemEntity> prepareOrderedItems(Set<OrderedItemRequest> orderedItemRequests, OrderEntity orderEntity) {
        Set<OrderedItemEntity> orderedItemsEntities = new HashSet<>();

        itemsProvider.doWithItems(orderedItemRequests,
                (itemEntity, orderedItemRequest) -> orderedItemsEntities.add(
                        OrderedItemEntity
                                .builder()
                                .price(itemEntity.getPrice())
                                .quantity(orderedItemRequest.getQuantity())
                                .itemEntity(itemEntity)
                                .orderEntity(orderEntity)
                                .discountEntities(new HashSet<>(itemEntity.getDiscountEntities()))
                                .build()));

        return orderedItemsEntities;
    }

    void updateItemsQuantity(Set<OrderedItemEntity> orderedItemEntities) {
        Set<ItemEntity> itemEntities = orderedItemEntities
                .stream()
                .map(orderedItemEntity -> {
                    ItemEntity itemEntity = orderedItemEntity.getItemEntity();
                    itemEntity.setQuantity(itemEntity.getQuantity() - orderedItemEntity.getQuantity());
                    return itemEntity;
                }).collect(Collectors.toSet());

        catalogFacade.saveAllItems(itemEntities);
    }
}
