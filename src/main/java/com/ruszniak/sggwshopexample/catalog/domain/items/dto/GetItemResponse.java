package com.ruszniak.sggwshopexample.catalog.domain.items.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GetItemResponse {

    private long id;

    private String name;

    private BigDecimal price;

    private String description;

    private long quantity;

    @Builder.Default
    private Set<DiscountInformation> discounts = new HashSet<>();
}
