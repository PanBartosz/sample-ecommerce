package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface ItemRepository extends JpaRepository<ItemEntity, Long> {
}
