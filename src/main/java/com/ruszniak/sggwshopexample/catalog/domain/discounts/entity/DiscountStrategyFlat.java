package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class DiscountStrategyFlat extends DiscountStrategy {

    public DiscountStrategyFlat() {
        discountType = DiscountType.FLAT;
    }

    @Override
    public BigDecimal apply(BigDecimal itemPrice, BigDecimal discountAmount, BigDecimal totalOrderPrice) {
        return totalOrderPrice
                .subtract(discountAmount);
    }
}
