package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFactorDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFlatDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.GetDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemRequest;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.GetItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Set;

@AllArgsConstructor
public class CatalogFacade {

    private final ItemService itemService;
    private final DiscountService discountService;


    public ItemEntity getItemById(Long id) {
        return itemService.getItemById(id);
    }

    public void saveAllItems(Set<ItemEntity> items) {
        itemService.saveAll(items);
    }

    public boolean itemExistsById(Long id) {
        return itemService.existsById(id);
    }

    public List<GetItemResponse> getAllItemResponses() {
        return itemService.getAllResponses();
    }

    public GetItemResponse getItemResponseById(Long id) {
        return itemService.getResponseById(id);
    }

    public CreateItemResponse createItem(CreateItemRequest request) {
        return itemService.createItem(request);
    }

    public List<ItemEntity> getItemEntitiesById(List<Long> ids) {
        return itemService.findAllById(ids);
    }

    public CreateDiscountResponse createDiscount(CreateFlatDiscountRequest request) {
        return discountService.persistRequestCreate(request);
    }

    public CreateDiscountResponse createDiscount(CreateFactorDiscountRequest request) {
        return discountService.persistRequestCreate(request);
    }

    public List<GetDiscountResponse> getAllDiscountResponses() {
        return discountService.getAllResponses();
    }

    public GetDiscountResponse getDiscountResponseById(Long id) {
        return discountService.getResponseById(id);
    }
}
