package com.ruszniak.sggwshopexample.catalog.domain.items.dto;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountType;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DiscountInformation {

    private BigDecimal amount;

    private DiscountType type;
}
