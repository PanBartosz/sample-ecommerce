package com.ruszniak.sggwshopexample.catalog.domain.items.dto;

import com.ruszniak.sggwshopexample.core.dto.Request;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateItemRequest implements Request {

    @NotNull
    private String name;

    @Min(0)
    @NotNull
    private BigDecimal price;

    private String description;

    @Min(0)
    @NotNull
    private long quantity;
}
