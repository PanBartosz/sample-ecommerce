package com.ruszniak.sggwshopexample.catalog.domain.discounts.dto;

import com.ruszniak.sggwshopexample.core.dto.Request;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateFactorDiscountRequest implements Request {

    @Max(100)
    private BigDecimal amount;

    private Set<Long> items = new HashSet<>();
}
