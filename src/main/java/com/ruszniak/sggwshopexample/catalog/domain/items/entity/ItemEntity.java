package com.ruszniak.sggwshopexample.catalog.domain.items.entity;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "items")
public class ItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private BigDecimal price;

    private String description;

    private long quantity;

    @Builder.Default
    @ManyToMany
    @JoinTable(
            name = "discounts_items",
            joinColumns = @JoinColumn(name = "items_id"),
            inverseJoinColumns = @JoinColumn(name = "discounts_id")
    )
    private Set<DiscountEntity> discountEntities = new HashSet<>();
}
