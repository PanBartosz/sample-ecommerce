package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

public enum DiscountType {
    FLAT,
    FACTOR
}
