package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
public class DiscountStrategyFactor extends DiscountStrategy {

    public DiscountStrategyFactor() {
        discountType = DiscountType.FACTOR;
    }

    @Override
    public BigDecimal apply(BigDecimal itemPrice, BigDecimal discountAmount, BigDecimal totalOrderPrice) {
        return totalOrderPrice
                .subtract(itemPrice.multiply(discountAmount))
                .divide(new BigDecimal(100), RoundingMode.HALF_EVEN);
    }

}
