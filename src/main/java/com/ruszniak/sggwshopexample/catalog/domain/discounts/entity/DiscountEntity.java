package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "discounts")
public class DiscountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Convert(converter = DiscountStrategyConverter.class)
    private DiscountStrategy discountStrategy;

    private BigDecimal amount;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "discounts_items",
            joinColumns = @JoinColumn(name = "discounts_id"),
            inverseJoinColumns = @JoinColumn(name = "items_id")
    )
    private Set<ItemEntity> itemEntities = new HashSet<>();
}
