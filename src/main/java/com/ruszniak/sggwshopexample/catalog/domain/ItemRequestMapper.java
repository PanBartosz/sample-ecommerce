package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemRequest;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class ItemRequestMapper {

    ItemEntity mapToEntity(CreateItemRequest createItemRequest) {
        return ItemEntity
                .builder()
                .name(createItemRequest.getName())
                .price(createItemRequest.getPrice())
                .description(createItemRequest.getDescription())
                .quantity(createItemRequest.getQuantity())
                .build();
    }
}
