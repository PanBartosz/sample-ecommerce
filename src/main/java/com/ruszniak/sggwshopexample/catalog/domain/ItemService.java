package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemRequest;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.GetItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import com.ruszniak.sggwshopexample.core.exception.ObjectNotFoundException;
import com.ruszniak.sggwshopexample.core.validation.ValidationProcessor;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
class ItemService {

    private final ItemRepository itemRepository;
    private final ItemRequestMapper requestMapper;
    private final ItemResponseMapper responseMapper;
    private final ValidationProcessor validationProcessor;

    CreateItemResponse createItem(CreateItemRequest request) {
        validationProcessor.processRequest(request);

        ItemEntity itemEntity = requestMapper.mapToEntity(request);
        itemEntity = itemRepository.save(itemEntity);
        return responseMapper.mapToResponseCreate(itemEntity);
    }

    List<GetItemResponse> getAllResponses() {
        return itemRepository
                .findAll()
                .stream()
                .map(responseMapper::mapToResponseGet)
                .collect(Collectors.toList());
    }

    GetItemResponse getResponseById(Long id) {
        ItemEntity itemEntity = itemRepository
                .findById(id)
                .orElseThrow(ObjectNotFoundException::new);

        return responseMapper.mapToResponseGet(itemEntity);
    }

    List<ItemEntity> findAllById(List<Long> ids) {
        return itemRepository.findAllById(ids);
    }

    ItemEntity getItemById(Long id) {
        return itemRepository
                .findById(id)
                .orElseThrow(ObjectNotFoundException::new);
    }

    void saveAll(Set<ItemEntity> items) {
        itemRepository.saveAll(items);
    }

    boolean existsById(Long id) {
        return itemRepository.existsById(id);
    }
}
