package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.core.validation.ValidationProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CatalogConfig {

    @Bean
    CatalogFacade getCatalogFacade(
            ItemRepository itemRepository,
            DiscountRepository discountRepository,
            ValidationProcessor validator) {

        return new CatalogFacade(
                new ItemService(
                        itemRepository,
                        new ItemRequestMapper(),
                        new ItemResponseMapper(),
                        validator),
                new DiscountService(
                        discountRepository,
                        new DiscountRequestMapper(itemRepository),
                        new DiscountResponseMapper(),
                        validator));
    }
}
