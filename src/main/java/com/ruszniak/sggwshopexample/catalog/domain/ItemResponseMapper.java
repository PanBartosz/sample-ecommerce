package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.DiscountInformation;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.GetItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;

import java.util.stream.Collectors;

@AllArgsConstructor
class ItemResponseMapper {

    CreateItemResponse mapToResponseCreate(ItemEntity itemEntity) {
        return CreateItemResponse
                .builder()
                .id(itemEntity.getId())
                .name(itemEntity.getName())
                .price(itemEntity.getPrice())
                .description(itemEntity.getDescription())
                .quantity(itemEntity.getQuantity())
                .build();
    }

    GetItemResponse mapToResponseGet(ItemEntity itemEntity) {
        return GetItemResponse
                .builder()
                .id(itemEntity.getId())
                .name(itemEntity.getName())
                .price(itemEntity.getPrice())
                .description(itemEntity.getDescription())
                .quantity(itemEntity.getQuantity())
                .discounts(itemEntity.getDiscountEntities().stream()
                        .map(discountEntity -> DiscountInformation.builder()
                                .amount(discountEntity.getAmount())
                                .type(discountEntity.getDiscountStrategy().getDiscountType())
                                .build())
                        .collect(Collectors.toSet()))
                .build();
    }
}
