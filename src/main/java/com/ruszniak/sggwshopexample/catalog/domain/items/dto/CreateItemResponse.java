package com.ruszniak.sggwshopexample.catalog.domain.items.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class CreateItemResponse {

    private long id;

    private String name;

    private BigDecimal price;

    private String description;

    private long quantity;
}
