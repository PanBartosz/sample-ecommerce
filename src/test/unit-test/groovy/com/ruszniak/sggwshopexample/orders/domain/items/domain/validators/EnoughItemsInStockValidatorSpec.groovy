package com.ruszniak.sggwshopexample.orders.domain.items.domain.validators

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade
import com.ruszniak.sggwshopexample.orders.domain.validators.EnoughItemsInStockValidator
import spock.lang.Specification
import spock.lang.Unroll

import javax.validation.ConstraintValidatorContext

class EnoughItemsInStockValidatorSpec extends Specification {

    def catalogFacade = Mock(CatalogFacade)
    def systemUnderTest = new EnoughItemsInStockValidator(catalogFacade)
    def fixture = EnoughItemsInStockValidatorFixture.instance

    @Unroll
    def 'items in stock: #itemEntityQuantity ordered items: #orderedItemQuantity is there enough items for the order: #expectedResult'() {
        given:
            def requestUnderTest = fixture.prepareOrderedItemRequest(orderedItemQuantity)
            catalogFacade.getItemById(1) >> fixture.prepareItemEntity(itemEntityQuantity)

        expect:
            def result = systemUnderTest.isValid(requestUnderTest, (ConstraintValidatorContext) null)
            result == expectedResult

        where:
            orderedItemQuantity | itemEntityQuantity || expectedResult
            10                  | 20                 || true
            20                  | 10                 || false
            10                  | 10                 || true
    }

}
