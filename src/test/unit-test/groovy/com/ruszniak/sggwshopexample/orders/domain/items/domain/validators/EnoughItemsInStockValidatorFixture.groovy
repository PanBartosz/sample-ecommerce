package com.ruszniak.sggwshopexample.orders.domain.items.domain.validators

import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemRequest

@Singleton
class EnoughItemsInStockValidatorFixture {

    def prepareOrderedItemRequest(Long quantity) {
        OrderedItemRequest
                .builder()
                .id(1)
                .quantity(quantity)
                .build()
    }

    def prepareItemEntity(Long quantity) {
        ItemEntity
                .builder()
                .id(1)
                .quantity(quantity)
                .build()
    }
}
