package utils


import lombok.NoArgsConstructor

import static com.google.common.io.Resources.getResource
import static com.google.common.io.Resources.toString
import static java.nio.charset.StandardCharsets.UTF_8
import static lombok.AccessLevel.PRIVATE

@NoArgsConstructor(access = PRIVATE)
class JsonLoader {

    static String loadFileFromResource(String filePath) throws IOException {
        return toString(getResource('json/' + filePath), UTF_8)
    }
}
