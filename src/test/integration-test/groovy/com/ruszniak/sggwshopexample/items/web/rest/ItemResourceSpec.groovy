package com.ruszniak.sggwshopexample.items.web.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.ruszniak.sggwshopexample.Runner
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import utils.JsonLoader

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = MOCK)
@ContextConfiguration(classes = [Runner])
class ItemResourceSpec extends Specification {

    @Autowired
    private MockMvc mvc

    @Autowired
    private ObjectMapper objectMapper

    def 'should create item'() {
        given:
            def requestUnderTest = JsonLoader.loadFileFromResource('items/CreateItemRequest.json')

        when:
            def result = mvc
                    .perform(post('/api/items')
                    .contentType(APPLICATION_JSON)
                    .content(requestUnderTest))
                    .andExpect(status().isOk())
                    .andReturn()
                    .response

        then:
            def response = objectMapper.readValue(result.contentAsString, CreateItemResponse)
            response.name == 'item'
            response.price == new BigDecimal(20)
            response.description == 'description'
            response.quantity == 20
    }
}
