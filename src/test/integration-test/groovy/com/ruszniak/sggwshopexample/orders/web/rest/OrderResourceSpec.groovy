package com.ruszniak.sggwshopexample.orders.web.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.ruszniak.sggwshopexample.Runner
import com.ruszniak.sggwshopexample.orders.domain.dto.CreateOrderResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import utils.JsonLoader

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = MOCK)
@Sql(scripts = "/sql/orders/CreateItems", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ContextConfiguration(classes = [Runner])
class OrderResourceSpec extends Specification { // todo add test for closing an order with items that are discounted

    @Autowired
    private MockMvc mvc

    @Autowired
    private ObjectMapper objectMapper

    private def fixture = OrderResourceFixture.instance

    def 'should create order with item'() {
        given:
            def requestUnderTest = JsonLoader.loadFileFromResource('orders/CreateOrderRequest.json')
            def expectedResponse = fixture.prepareExpectedCreateOrderResponse()

        when:
            def result = mvc
                    .perform(post('/api/orders')
                    .contentType(APPLICATION_JSON)
                    .content(requestUnderTest))
                    .andExpect(status().isOk())
                    .andReturn()
                    .response

        then:
            def response = objectMapper.readValue(result.contentAsString, CreateOrderResponse)
            response.address == expectedResponse.address
            response.price == expectedResponse.price
            response.orderStatus == expectedResponse.orderStatus
            response.orderedItemResponse[0].price == expectedResponse.orderedItemResponse[0].price
            response.orderedItemResponse[0].description == expectedResponse.orderedItemResponse[0].description
            response.orderedItemResponse[0].quantity == expectedResponse.orderedItemResponse[0].quantity
            response.orderedItemResponse[0].name == expectedResponse.orderedItemResponse[0].name
    }
}
