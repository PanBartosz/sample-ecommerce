package com.ruszniak.sggwshopexample.orders.web.rest

import com.ruszniak.sggwshopexample.orders.domain.dto.CreateOrderResponse
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemResponse
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderStatus

@Singleton
class OrderResourceFixture {

    private def prepareExpectedOrderedItemResponses() {
        def responses = new ArrayList<OrderedItemResponse>()
        responses.add(new OrderedItemResponse(
                name: 'test item 1',
                price: BigDecimal.TEN,
                description: 'description for test item 1',
                quantity: 20))

        responses
    }

    def prepareExpectedCreateOrderResponse() {
        CreateOrderResponse
                .builder()
                .address('test order address')
                .price(BigDecimal.TEN)
                .orderStatus(OrderStatus.CREATED)
                .orderedItemResponse(prepareExpectedOrderedItemResponses())
                .build()
    }
}
