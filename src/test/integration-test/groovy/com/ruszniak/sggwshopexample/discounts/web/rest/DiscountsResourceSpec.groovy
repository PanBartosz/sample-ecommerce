package com.ruszniak.sggwshopexample.discounts.web.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.ruszniak.sggwshopexample.Runner
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import utils.JsonLoader

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = MOCK)
@Sql(scripts = "/sql/discounts/CreateItems", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ContextConfiguration(classes = [Runner])
class DiscountsResourceSpec extends Specification {

    @Autowired
    private MockMvc mvc

    @Autowired
    private ObjectMapper objectMapper

    private def fixture = DiscountResourceFixture.instance

    def 'should create flat discount with items'() {
        given:
            def requestUnderTest = JsonLoader.loadFileFromResource('discounts/CreateDiscountRequest.json')
            def expectedResponse = fixture.prepareExpectedDiscountResponse(DiscountType.FLAT)

        when:
            def result = mvc
                    .perform(post('/api/discounts/flat')
                    .contentType(APPLICATION_JSON)
                    .content(requestUnderTest))
                    .andExpect(status().isOk())
                    .andReturn()
                    .response

        then:
            def response = objectMapper.readValue(result.contentAsString, CreateDiscountResponse)
            response.amount == expectedResponse.amount
            response.type == expectedResponse.type
            response.items.containsAll(expectedResponse.items)
    }

    def 'should create factor discount with items'() {
        given:
            def requestUnderTest = JsonLoader.loadFileFromResource('discounts/CreateDiscountRequest.json')
            def expectedResponse = fixture.prepareExpectedDiscountResponse(DiscountType.FACTOR)

        when:
            def result = mvc
                    .perform(post('/api/discounts/factor')
                    .contentType(APPLICATION_JSON)
                    .content(requestUnderTest))
                    .andExpect(status().isOk())
                    .andReturn()
                    .response

        then:
            def response = objectMapper.readValue(result.contentAsString, CreateDiscountResponse)
            response.amount == expectedResponse.amount
            response.type == expectedResponse.type
            response.items.containsAll(expectedResponse.items)
    }
}
