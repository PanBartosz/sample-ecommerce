package com.ruszniak.sggwshopexample.discounts.web.rest

import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountType

@Singleton
class DiscountResourceFixture {

    private def prepareExpectedItemIds() {
        def ids = new HashSet()
        ids.add(new Long(1))
        ids.add(new Long(2))

        ids
    }

    def prepareExpectedDiscountResponse(DiscountType type) {
        CreateDiscountResponse
                .builder()
                .type(type)
                .amount(BigDecimal.TEN)
                .items(prepareExpectedItemIds())
                .build()
    }
}
